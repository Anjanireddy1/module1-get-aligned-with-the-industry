**Neural Networks**

A neural network (NN) consists of many simple, connected processors called neurons, each producing a sequence of real-valued activations.

![Alt Text](https://img.securityinfowatch.com/files/base/cygnus/siw/image/2019/02/Figure_01.5c7712513151e.png?auto=format&w=720)


** Deep Learning**

Deep learning is a sub-field of machine learning dealing with algorithms inspired by the structure and function of the brain called artificial neural networks.

![Alt Text](https://insidebigdata.com/wp-content/uploads/2018/10/Neural-Network-diagram.jpg)


Deep learning models work in layers and a typical model atleast have three layers. Each layer accepts the information from previous and pass it on to the next one.

![Alt Text](https://miro.medium.com/max/700/0*a-bfJPbzADKVlFfv.png)


**Important concepts of neural networks**

*Training*

Weights start out as random values, and as the neural network learns more about what kind of input data leads to a student being accepted into a university(above example), the network adjusts the weights based on any errors in categorization that the previous weights resulted in. This is called training the neural network.

*Error*

This very important concept to define how well a network performing during the training. In the training phase of the network, it make use of error value to adjust the weights so that it can get reduced error at each step. The goal of the training phase to minimize the error

**Gradient Descent**

- Gradient descent is an optimization algorithm used to find the values of parameters (coefficients) of a function (f) that minimizes a cost function (cost).

- Gradient descent is best used when the parameters cannot be calculated analytically (e.g. using linear algebra) and must be searched for by an optimization algorithm.Gradient descent is used to find the minimum error by minimizing a “cost” function.

**Back Propagation**

- In neural networks, you forward propagate to get the output and compare it with the real value to get the error. 

- To minimise the error, you propagate backwards by finding the derivative of error with respect to each weight and then subtracting this value from the weight value. This is called back propagation.

*Here is the back propagation algorithm from Udacity:*

![Alt Text](https://miro.medium.com/max/700/1*Zg2vhJr8QybZAISCG25SyQ.png)


